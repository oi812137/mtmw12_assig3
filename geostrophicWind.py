#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct 15 09:38:04 2021

MTMQ12 assignment 3. Hilary Weller 1 October 2020
Python3 code to numerically differentiate the pressure in order to calculate
the geostrophic wind relation using 2-point differencing and compare with the
analytic solution and plot

@author: David Griffiths
"""

import numpy as np
import matplotlib.pyplot as plt
from differentiate import *
from physProps import *
from plot import *

def geoStrophicWind(N, method):
    """Calculate the geostrophic wind analytically, numerically, and plot     

    Parameters
    ----------
    N : the number of intervals to divide space into
    method: 2 => centred difference, 5 => five point method
    
    Returns
    -------
    y :  array of y values corresponding to errors array
    errors : array of arithmetic errors compared to the analytic result"""
 
    ymin = physProps["ymin"]
    ymax = physProps["ymax"]
    dy = (ymax - ymin)/N  # The length of the spacing
    
    # The spatial dimension, y
    y = np.linspace(ymin, ymax, N+1)
    
    # The pressure at the y points and the exact geostrophic wind
    p = pressure(y, physProps)
    uExact = uGeoExact(y, physProps)
    
    # The pressure gradient and wind using difference method
    if method == 2:
        dpdy = gradient_2point(p, dy)
    else:
        dpdy = gradient_5point(p, dy)
        
    u_finiteDifference = geoWind(dpdy, physProps)
    
    # The arithmetic errors of the numerical solution
    errors = uExact - u_finiteDifference
    
    PlotResults(y, method, uExact, u_finiteDifference)
    
    return y, errors
       
def Main():
    """ 
    Main execution function for assignment
    """
    
    
    # Calculate wind using N=10 and second order finite difference method and
    # plot results
    y10, error10 = geoStrophicWind(10, 2)
    
    # Plot errors at all 11 points
    PlotLine(y10, error10, 'Error in u (m/s)', 'Errors',\
             'geoWindCentreError.pdf')
    
    #Strip out mid-point errors and plot
    y10mid = y10[1:10]
    error10mid = error10[1:10] 
    PlotLine(y10mid, error10mid, 'Error in u (m/s)', 'Errors', \
             'geoWindCentreErrorMidPoints.pdf')
    
    ##################################################
    #   Prove that for mid-points, error = O(dy^2)   #
    ##################################################
    
    #Perform N = 20 (second order finite diff) and retrieve arithmetic errors
    y20, error20 = geoStrophicWind(20, 2)

    #Strip out data that corresponds to mid-point values of y calculated 
    #previously where N was equal to 10
    y20mid = y20[1:20]
    error20mid = error20[1:20]    
    error20compare = np.zeros_like(error10mid)

    for i in range(len(y10mid)):
        error20compare[i] = error20mid[(2*i) + 1]
            
    #Calculate factor by which errors have reduced, and plot
    errorFactors =   error20compare / error10mid
    
    PlotLine(y10mid, errorFactors.round(9), 'Error Factors', \
             'Error Factors', 'geoWindErrorFactorsMidPoints.pdf')
        
    #Prove that for end-points, error = O(dy)

    #Strip out end-point data
    y10end = np.array([0, y10[-1]])
    error10end = np.array([error10[0], error10[-1]])
    error20end = np.array([error20[0], error20[-1]])
            
    #Calculate factor by which errors have reduced, and plot
    errorFactors = error20end / error10end
    
    PlotLine(y10end, errorFactors.round(9), 'Error Factors', \
             'Error Factors', 'geoWindErrorFactorsEndPoints.pdf')
        
    ##################################################
    #           Perform five-point method            #
    ##################################################
    
    # Calculate wind, N=20, five-point difference method and plot results
    y20_5pt, error20_5pt = geoStrophicWind(20, 5)
    
    # Show that this method is more accurate. Calculate factor by which errors
    # have reduced, and plot
    errorReduction = np.zeros(17)
    yErrorReduction = np.zeros(17)
    
    for i in range(17):
        yErrorReduction[i] = y20_5pt[i+2]
        errorReduction[i] = abs(error20_5pt[i+2]/error20[i+2])
    
    PlotLine(yErrorReduction, errorReduction.round(7), 'Error Factors', \
             'Error Factor', 'geoWindErrorFactors5Points.pdf')
    
if __name__ == "__main__":
    Main()
    
