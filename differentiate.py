#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct 15 09:38:04 2021

File contains methods to differentiate using finite difference methods

@author: David Griffiths
"""

import numpy as np

def gradient_2point(f, dx):
    """The gradient of one dimensional array f assuming points are a distance
    dx apart using 2-point differences. Returns an array the same size as f"""
    
    #Initialised the array for the gradient to be the same size as f
    dfdx = np.zeros_like(f)
    
    #Two point differences at the end points 
    dfdx[0] = (f[1]-f[0])/dx
    dfdx[-1] = (f[-1]-f[-2])/dx

    #Centred differences for the mid-points
    for i in range(1, len(f)-1):
        dfdx[i] = (f[i+1]-f[i-1])/(2*dx)
 
    return dfdx

def gradient_5point(f, dx):
    """The gradient of one dimensional array f assuming points are a distance
    dx apart using 5-point differences. Returns an array the same size as f"""
    
    #Initialised the array for the gradient to be the same size as f
    dfdx = np.zeros_like(f)
    
    #Two point differences at the end points 
    dfdx[0] = (f[1]-f[0])/dx
    dfdx[-1] = (f[-1]-f[-2])/dx

    #Centred differences for the penultimate points
    dfdx[1] = (f[2]-f[0])/(2*dx)
    dfdx[-2] = (f[-1]-f[-3])/(2*dx)
    
    #Five-point differences for the mid-points
    for i in range(2, len(f)-2):
        dfdx[i] = (-f[i+2]+8*f[i+1]-8*f[i-1]+f[i-2])/(12*dx)
 
    return dfdx