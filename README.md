To run code and reproduce results:
- Download all .py files from project and open in Spyder (Python 3.8)
- If PrettyTable has not already been installed execute the following command
  in the Spyder console: 
  
  conda install -c conda-forge prettytable
  
- Open geoStrophicWind.py and execute 
