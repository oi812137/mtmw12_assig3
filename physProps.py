#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct 15 09:38:04 2021

File contains methods to calculate analytical properties based for wind

@author: David Griffiths
"""

import numpy as np

# A dictionary of physical properties
physProps = {'pa' : 1e5,    #The mean pressure
             'pb' : 200,    #The magnitude of the pressure variations
             'f'  : 1e-4,   #The Coriolis parameter
             'rho': 1.,     #The density
             'L'  : 2.4e6,  #Lenth scale of the pressure variations (k)
             'ymin': 0.,    #Start of the y domain
             'ymax': 1e6}   #End of the y domain


def pressure(y, props):
    """The pressure and given y locations based on dictionary of physical
    properties, props"""
    
    pa = props["pa"]
    pb = props["pb"]
    L = props["L"]
    return pa + pb*np.cos(y*np.pi/L)

def uGeoExact(y, props):
    """The analytic geostropic wind at given locations, y based on dictionary
    of physical properties, props"""
    pb = props["pb"]
    L = props["L"]
    rho = props["rho"]
    f = props["f"]
   
    return pb*np.pi/(rho*f*L)*np.sin(y*np.pi/L)

def geoWind(dpdy, props):
    """The geostrophic wind as a function of pressure gradient based on 
    dictionary of physcial properties, props"""
    rho = props["rho"]
    f = props["f"]
    return -dpdy/(rho*f)