#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct 15 21:32:22 2021

File contains methods to plot data in graphs

@author: david
"""

import matplotlib.pyplot as plt

def PlotResults(y, method, uExact, uNumeric):
    """ Plot two lines on graph with custom formatting
    
    Parameters
    ----------
    y : array of y values
    method : 2 => 2 point method, 4 => 4 point method
    uExact : array of analytical results
    uNumeric : array of numerically calculated result """
    
    # Graph to compare the numerical and analytic solutions
    # Plot using large fonts
    font = {'size' : 14}
    plt.rc('font', **font)
    
    methodLabel = 'Two-point differences' if method == 2 else \
        'Five-point differences'
    
    # Plot the approximate and exact wind at y points
    plt.plot(y/1000, uExact, 'k-', label='Exact')
    plt.plot(y/1000, uNumeric, '*k--', label=methodLabel, \
             ms=12, markeredgewidth=1.5, markerfacecolor='none')
    plt.legend(loc='best')
    plt.xlabel('y (km)')
    plt.ylabel('u (m/s)')
    plt.tight_layout()
    plt.savefig('geoWindCentr.pdf')
    plt.show()
    
def PlotLine(y, data, ylabel, linelabel, outputFilename):
    """Plot one line on graph with custom formatting
    
    Parameters
    ----------
    y : array of y values
    data :  data to plot
    ylabel : label for y-axis
    linelabel : label for line
    outputFilename : filename for pdf of results"""
     
    font = {'size' : 14}
    plt.rc('font', **font)
    
    plt.plot(y/1000, data, 'k-', label=linelabel)
    plt.legend(loc='best')
    plt.xlabel('y (km)')
    plt.ylabel(ylabel)
    plt.tight_layout()
    plt.savefig(outputFilename)
    plt.show()